package example3;

import java.util.ArrayList;

public interface BoardConstants {
    ArrayList<Figure> FIGURES_LIST = new ArrayList<>();
    int BLACK = 0;
    int WHITE = 1;
}
