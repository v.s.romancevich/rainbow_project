package example3;

import javax.swing.*;
import java.awt.*;

public class ChessBoard extends JPanel implements BoardConstants {
    public ChessBoard() {
        ChessEngine engine = new ChessEngine(this);
        addMouseListener(engine);
        addMouseMotionListener(engine);
    }

    @Override
    public void paint(Graphics g) {
        boolean white = true;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (white) {
                    g.setColor(Color.WHITE.darker());
                } else {
                    g.setColor(Color.ORANGE.brighter());
                }
                white = !white;
                g.fillRect(j * 64, i * 64, 64, 64);
            }
            white = !white;
        }

        for (Figure currentFigure : FIGURES_LIST) {
            g.drawImage(currentFigure.image, currentFigure.x * 64, currentFigure.y * 64, this);
        }
    }
}
