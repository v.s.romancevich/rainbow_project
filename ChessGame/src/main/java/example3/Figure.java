package example3;

import java.awt.*;
import java.util.Objects;

public class Figure {
    int x;
    int y;
    Image image;
    int team;

    public Figure(int x, int y, Image image, int team) {
        this.x = x;
        this.y = y;
        this.image = image;
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Figure figure = (Figure) o;
        return x == figure.x && y == figure.y && team == figure.team && Objects.equals(image, figure.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, image, team);
    }
}
