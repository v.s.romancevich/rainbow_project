package example3;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class ChessEngine implements MouseMotionListener, MouseListener, BoardConstants {
    Figure selectedFigure;
    ChessBoard chessBoard;

    public ChessEngine(ChessBoard board) {
        chessBoard = board;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (selectedFigure != null) {
            selectedFigure.x = e.getX() / 64;
            selectedFigure.y = e.getY() / 64;
            chessBoard.repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        selectedFigure = getFigure(e.getX() / 64, e.getY() / 64);
    }

    @Override
    public void mouseReleased(MouseEvent e) {


        chessBoard.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    private Figure getFigure(int xFigure, int yFigure) {

        for (Figure currentFigure : FIGURES_LIST) {
            if (currentFigure.x == xFigure && currentFigure.y == yFigure) {
                return currentFigure;
            }
        }
        return null;
    }
}
