package example3;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageParser {
    private BufferedImage bufferedImage;
    private final Image[][] images;

    public ImageParser() {
        try {
            String path = "tileset4.png";
            bufferedImage = ImageIO.read(new File(path));
        } catch (IOException e) {
            System.err.println("������ �������� ����� � ����������");
        }

        images = new Image[2][6];
    }

    public void parseIt() {
        int index;
        for (int i = 0; i < 266; i += 133) {
            int x = i == 0 ? 0 : 1;
            index = 0;
            for (int j = 0; j < 798; j += 133) {
                images[x][index] = bufferedImage.getSubimage(j, i, 133, 133)
                        .getScaledInstance(64, 64, BufferedImage.SCALE_SMOOTH);
                index++;
            }
        }
    }

    public Image getImage(int indexX, int indexY) {
        return images[indexX][indexY];
    }

}
