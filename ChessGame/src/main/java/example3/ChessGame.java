package example3;

public class ChessGame implements BoardConstants {
    public void initGame() {
        ImageParser imageParser = new ImageParser();
        imageParser.parseIt();

        for (int i = 0; i < 8; i++) {
            FIGURES_LIST.add(new Figure(i, 1,
                    imageParser.getImage(BLACK, 5), BLACK));
            FIGURES_LIST.add(new Figure(i, 6,
                    imageParser.getImage(WHITE, 5), WHITE));
        }

        for (int i = 0; i < 2; i++) {
            int team = i == 0 ? BLACK : WHITE;
            int y = i == 0 ? 0 : 7;
            FIGURES_LIST.add(new Figure(7, y, imageParser.getImage(team, 4), team));
            FIGURES_LIST.add(new Figure(6, y, imageParser.getImage(team, 3), team));
            FIGURES_LIST.add(new Figure(5, y, imageParser.getImage(team, 2), team));
            FIGURES_LIST.add(new Figure(4, y, imageParser.getImage(team, 0), team));
            FIGURES_LIST.add(new Figure(3, y, imageParser.getImage(team, 1), team));
            FIGURES_LIST.add(new Figure(2, y, imageParser.getImage(team, 2), team));
            FIGURES_LIST.add(new Figure(1, y, imageParser.getImage(team, 3), team));
            FIGURES_LIST.add(new Figure(0, y, imageParser.getImage(team, 4), team));
        }

        ChessFrame board = new ChessFrame();
    }
}
