package example3;

import javax.swing.*;

public class ChessFrame extends JFrame {
    public ChessFrame() {
        this.setTitle("ChessGame");
        this.setBounds(50, 50, 526, 548);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        ChessBoard chessBoard = new ChessBoard();
        getContentPane().add(chessBoard);

        this.setVisible(true);
    }
}
