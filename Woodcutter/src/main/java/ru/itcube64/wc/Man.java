package ru.itcube64.wc;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Man {
    private BufferedImage woodcutterImage;
    private Animation animationRun;
    private double x;
    private final double xRunningSpeed;
    private int running;

    public Man(double x) {
        try {
            woodcutterImage = ImageIO.read(new File("Woodcutter.png"));
            animationRun = new Animation("Woodcutter_run.png", 6, 60);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.x = x;
        this.xRunningSpeed = 0.2;
        this.running = 0;
    }

    public void draw(Graphics g, int panelWidth, int panelHeight) {
        int imageX = (int) x;
        int imageY = panelHeight - woodcutterImage.getHeight();

        if (running == 0) {
            g.drawImage(woodcutterImage, imageX, imageY, null);
        } else if (running == 2) {
            imageY -= 50;
            g.drawImage(woodcutterImage, imageX, imageY, null);
        } else {
            boolean isMirrored = (running == -1);
            animationRun.draw(g, imageX, imageY, isMirrored);
        }
    }

    public void startRunningLeft() {
        running = -1;
    }

    public void startRunningRight() {
        running = 1;
    }

    public void startJump() {
        running = 2;
    }

    public void stopRunningLeft() {
        if (running == -1) {
            running = 0;
        }
    }

    public void stopRunningRight() {
        if (running == 1) {
            running = 0;
        }
    }

    public void stopJump() {
        if (running == 2) {
            running = 0;
        }
    }

    public void update(long dt) {
        x += dt * xRunningSpeed * running;

        animationRun.update(dt);
    }
}
