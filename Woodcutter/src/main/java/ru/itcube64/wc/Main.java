package ru.itcube64.wc;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        MyPanel panel = new MyPanel();

        JFrame frame = new JFrame();
        frame.setTitle("Woodcutter");
        frame.setSize(750, 410);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.add(panel);
        frame.setVisible(true);

        while (true) {
            frame.repaint();
            panel.updateWorldPhysics();
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
