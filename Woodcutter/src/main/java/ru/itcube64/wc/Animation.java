package ru.itcube64.wc;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Animation {
    private BufferedImage[] frames;
    private int frameWidth;
    private int frameHeight;

    private long timePerFrame;
    private long playingTime;

    public Animation(String path, int numberOfFrames, long timePerFrame) throws IOException {

        BufferedImage allFrames = ImageIO.read(new File(path));
        int allFrameWidth = allFrames.getWidth();

        this.frameWidth = allFrameWidth / numberOfFrames;
        this.frameHeight = allFrames.getHeight();

        frames = new BufferedImage[numberOfFrames];
        for (int i = 0; i < numberOfFrames; ++i) {
            frames[i] = allFrames.getSubimage(i * frameWidth, 0, frameWidth, frameHeight);
        }

        this.timePerFrame = timePerFrame;
        this.playingTime = 0;
    }

    public int getFrameWidth() {
        return frameWidth;
    }

    public int getFrameHeight() {
        return frameHeight;
    }

    public void restart() {
        playingTime = 0;
    }

    public void update(long dt) {
        long totalAnimationTime = timePerFrame * frames.length;
        playingTime = (playingTime + dt) % totalAnimationTime;
    }

    public void draw(Graphics g, int x, int y, boolean mirrored) {
        int i = (int) (playingTime / timePerFrame);
        BufferedImage currentFrame = frames[i];

        if (!mirrored) {
            g.drawImage(currentFrame, x, y, frameWidth, frameHeight, null);
        } else {
            g.drawImage(currentFrame, x, y, -frameWidth, frameHeight, null);
        }
    }
}
