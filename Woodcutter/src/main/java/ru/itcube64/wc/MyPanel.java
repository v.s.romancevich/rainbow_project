package ru.itcube64.wc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MyPanel extends JPanel implements KeyEventDispatcher {
    private Man man;
    private long previousWorldUpdateTime;

    public MyPanel() {
        this.man = new Man(200);
        this.previousWorldUpdateTime = System.currentTimeMillis();

        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
        //super.paintComponent(g);
        try {
            BufferedImage image = ImageIO.read(new File("fone.png"));
            g.drawImage(image, 0, 0, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        man.draw(g, this.getWidth(), this.getHeight());
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getID() == KeyEvent.KEY_PRESSED) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                man.startRunningLeft();
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                man.startRunningRight();
            }
        }

        if (e.getID() == KeyEvent.KEY_RELEASED) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                man.stopRunningLeft();
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                man.stopRunningRight();
            }
        }

        if (e.getID() == KeyEvent.KEY_PRESSED) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                man.startJump();
            }
        }

        if (e.getID() == KeyEvent.KEY_RELEASED) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                man.stopJump();
            }
        }

        return false;
    }

    void updateWorldPhysics() {
        long currentTime = System.currentTimeMillis();
        long dt = currentTime - previousWorldUpdateTime;

        man.update(dt);

        previousWorldUpdateTime = currentTime;
    }
}
