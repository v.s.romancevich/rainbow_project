package ru.itcube64.bg;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Random;

public class BubblePanel extends JPanel {
    int count = 0;
    public static final int DELAY = 10000;
    Random random = new Random();
    ArrayList<Bubble> bubbles;
    JFrame frame;

    public BubblePanel() {
        bubbles = new ArrayList<>();
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public void startGame() {
        while (true) {

            for (int i = 0; i < 10; i++) {
                int x = random.nextInt(550);
                int y = random.nextInt(550);

                bubbles.add(new Bubble(x, y));
            }

            repaint();

            try {
                Thread.sleep(DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void paintComponent(Graphics canvas) {
        canvas.clearRect(0, 0, 650, 650);

        for (Bubble bubble : bubbles) {
            bubble.draw(canvas);
        }
    }

    private class Bubble {
        private int x;
        private int y;
        private int size = 50;

        public Bubble(int x, int y) {
            this.x = x;
            this.y = y;

            addMouseListener(new BubbleListener(this));
        }

        public void draw(Graphics canvas) {
            canvas.setColor(Color.GREEN);
            canvas.fillOval(x, y, size, size);

            canvas.setColor(Color.RED);
            for (int i = 0; i < 50; i += 5) {
                canvas.drawOval(x, y, size - i, size - i);
            }

        }

        public void deleteBubble(int x, int y) {
            for (int i = bubbles.size() - 1; i > -1; i--) {
                double dx = bubbles.get(i).x + bubbles.get(i).size / 2 - x;
                double dy = bubbles.get(i).y + bubbles.get(i).size / 2 - y;
                double d = Math.sqrt(dx * dx + dy * dy);

                if (d < bubbles.get(i).size / 2) {
                    bubbles.remove(i);
                    count++;
                    break;
                }
            }

            repaint();
        }
    }

    private class BubbleListener extends MouseAdapter {
        Bubble bubble;

        public BubbleListener(Bubble bubble) {
            this.bubble = bubble;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            bubble.deleteBubble(e.getX(), e.getY());
            BubbleGame.score.setText("" + count);
        }
    }
}
