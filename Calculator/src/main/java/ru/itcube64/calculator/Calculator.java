package ru.itcube64.calculator;

import javax.swing.*;
import java.awt.*;

public class Calculator extends JFrame {
    JTextField displayField;

    JButton buttonPlus = new JButton("+");
    JButton buttonMinus = new JButton("-");
    JButton buttonDivide = new JButton("/");
    JButton buttonMultiply = new JButton("*");

    JButton button0 = new JButton("0");
    JButton button1 = new JButton("1");
    JButton button2 = new JButton("2");
    JButton button3 = new JButton("3");
    JButton button4 = new JButton("4");
    JButton button5 = new JButton("5");
    JButton button6 = new JButton("6");
    JButton button7 = new JButton("7");
    JButton button8 = new JButton("8");
    JButton button9 = new JButton("9");

    JButton buttonPoint = new JButton(".");
    JButton buttonEqual = new JButton("=");

    public Calculator() {
        CalculatorEngine engine = new CalculatorEngine(this);

        this.setTitle("Calculator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        BorderLayout borderLayout = new BorderLayout();
        this.setLayout(borderLayout);

        displayField = new JTextField(30);
        displayField.setBackground(Color.GRAY.brighter());
        displayField.setHorizontalAlignment(JTextField.RIGHT);
        this.add("North", displayField);

        JPanel panelNumbers = new JPanel();
        GridLayout gridLayout = new GridLayout(3, 4);
        panelNumbers.setLayout(gridLayout);

        panelNumbers.add(button0);
        panelNumbers.add(button1);
        panelNumbers.add(button2);
        panelNumbers.add(button3);
        panelNumbers.add(button4);
        panelNumbers.add(button5);
        panelNumbers.add(button6);
        panelNumbers.add(button7);
        panelNumbers.add(button8);
        panelNumbers.add(button9);
        panelNumbers.add(buttonPoint);
        panelNumbers.add(buttonEqual);

        this.add("Center", panelNumbers);

        JPanel panelOperation = new JPanel();
        GridLayout gridLayout1 = new GridLayout(4, 1);
        panelOperation.setLayout(gridLayout1);

        panelOperation.add(buttonPlus);
        panelOperation.add(buttonMinus);
        panelOperation.add(buttonDivide);
        panelOperation.add(buttonMultiply);

        this.add("East", panelOperation);

        button0.addActionListener(engine);
        button1.addActionListener(engine);
        button2.addActionListener(engine);
        button3.addActionListener(engine);
        button4.addActionListener(engine);
        button5.addActionListener(engine);
        button6.addActionListener(engine);
        button7.addActionListener(engine);
        button8.addActionListener(engine);
        button9.addActionListener(engine);
        buttonPlus.addActionListener(engine);
        buttonMinus.addActionListener(engine);
        buttonDivide.addActionListener(engine);
        buttonMultiply.addActionListener(engine);
        buttonPoint.addActionListener(engine);
        buttonEqual.addActionListener(engine);

        this.pack();
        this.setVisible(true);
    }
}
