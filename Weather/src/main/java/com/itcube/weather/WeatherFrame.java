package com.itcube.weather;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class WeatherFrame extends JFrame {
    JLabel labelTitle;
    JLabel labelWeather;
    JLabel lblTempValue;
    JLabel lblPressureValue;
    JLabel lblHumidityValue;
    JLabel lblWindValue;
    JLabel labelOne;
    JLabel labelTwo;
    JLabel labelThree;
    JLabel label4;
    JLabel label5;
    JLabel label6;

    public WeatherFrame() {
        this.setTitle("Weather");

        try {
            this.setIconImage(ImageIO.read(
                    Objects.requireNonNull(
                            WeatherFrame.class.getResourceAsStream("/resources/weather.png"))));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setUndecorated(true);
        this.setOpacity(0.7f);

        BorderLayout borderLayout = new BorderLayout();
        getContentPane().setLayout(borderLayout);

        JPanel panelNorth = new JPanel();
        GridLayout gridLayoutNorth = new GridLayout(3, 1);
        panelNorth.setLayout(gridLayoutNorth);

        labelTitle = new JLabel("");
        labelTitle.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        panelNorth.add(labelTitle);

        labelWeather = new JLabel("Сейчас: ");
        labelWeather.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        panelNorth.add(labelWeather);

        JSeparator separator = new JSeparator();
        panelNorth.add(separator);

        getContentPane().add("North", panelNorth);

        JPanel panelCenter = new JPanel();
        GridLayout gridLayoutCenter = new GridLayout(2, 4);
        panelCenter.setLayout(gridLayoutCenter);

        JLabel lblTemp = new JLabel("Температура:");
        lblTemp.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblTemp.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblTemp);

        lblTempValue = new JLabel();
        lblTempValue.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblTempValue.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblTempValue);

        JLabel lblPressure = new JLabel("Давление:");
        lblPressure.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblPressure.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblPressure);

        lblPressureValue = new JLabel();
        lblPressureValue.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblPressureValue.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblPressureValue);

        JLabel lblHumidity = new JLabel("Влажность:");
        lblHumidity.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblHumidity.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblHumidity);

        lblHumidityValue = new JLabel();
        lblHumidityValue.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblHumidityValue.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblHumidityValue);

        JLabel lblWind = new JLabel("Ветер:");
        lblWind.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblWind.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblWind);

        lblWindValue = new JLabel();
        lblWindValue.setFont(new Font("Arial", Font.BOLD | Font.ITALIC, 15));
        lblWindValue.setHorizontalAlignment(SwingConstants.LEFT);
        panelCenter.add(lblWindValue);

        getContentPane().add("Center", panelCenter);

        JPanel panelSouth = new JPanel();
        GridLayout gridLayoutSouth = new GridLayout(2, 1);
        panelSouth.setLayout(gridLayoutSouth);

        JSeparator separatorSouth = new JSeparator();
        panelSouth.add(separatorSouth);

        JPanel panelBox = new JPanel();
        GridLayout grid = new GridLayout(2, 3);
        panelBox.setLayout(grid);

        labelOne = new JLabel();
        panelBox.add(labelOne);
        labelTwo = new JLabel();
        panelBox.add(labelTwo);
        labelThree = new JLabel();
        panelBox.add(labelThree);

        label4 = new JLabel();
        panelBox.add(label4);
        label5 = new JLabel();
        panelBox.add(label5);
        label6 = new JLabel();
        panelBox.add(label6);

        panelSouth.add(panelBox);

        getContentPane().add("South", panelSouth);

        this.pack();
    }

    public void init(ArrayList<Weather> weathers) {

        labelTitle.setText(weathers.get(0).title);
        labelWeather.setText("Сейчас: " + weathers.get(0).weather);
        lblTempValue.setText(weathers.get(0).temperature + "C");
        lblPressureValue.setText(weathers.get(0).pressure + " mm P.C.");
        lblHumidityValue.setText(weathers.get(0).humidity);
        lblWindValue.setText(weathers.get(0).wind);
        labelOne.setText(parseTitle(weathers.get(1).title));
        labelTwo.setText(parseTitle(weathers.get(2).title));
        labelThree.setText(parseTitle(weathers.get(3).title));
        label4.setText("T:" + weathers.get(1).temperature +
                ", В:" + weathers.get(1).wind);
        label5.setText("T:" + weathers.get(2).temperature +
                ", В:" + weathers.get(2).wind);
        label6.setText("T:" + weathers.get(3).temperature +
                ", В:" + weathers.get(3).wind);

        this.pack();

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension size = this.getSize();
        Insets scnMax = Toolkit.getDefaultToolkit().getScreenInsets(this.getGraphicsConfiguration());
        int taskBarSize = scnMax.bottom;
        Point p = new Point();
        p.setLocation(screenSize.getWidth() - size.getWidth(), screenSize.getHeight() - size.getHeight() - taskBarSize);
        this.setLocation(p);

        this.setVisible(true);
    }

    private String parseTitle(String title) {
        String[] arrayTitle = title.split(",");
        arrayTitle[0] = splitDayOfTheWeek(arrayTitle[0]);
        return arrayTitle[0] + ", " + arrayTitle[1] + ", " + arrayTitle[2];
    }

    private String splitDayOfTheWeek(String s) {
        switch (s) {
            case "Понедельник" -> s = "Пн";

            case "Вторник" -> s = "Вт";

            case "Среда" -> s = "Ср";

            case "Четверг" -> s = "Чт";

            case "Пятница" -> s = "Пт";

            case "Суббота" -> s = "Сб";

            case "Воскресенье" -> s = "Вс";

        }

        return s;
    }
}
