package com.itcube.weather;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class HTMLParser {
    private Weather parseFromHTML(Element newElement, String caption) {
        // подзаголовок
        String partText = "" + caption + ", " + newElement.selectFirst("td[class=partText]").text().toLowerCase();
        // температура
        String temp;
        if (newElement.selectFirst("td[class=temp]") == null) {
            temp = newElement.selectFirst("td[class=temp dayMain]").ownText();
        } else {
            temp = newElement.selectFirst("td[class=temp]").ownText();
        }
        // картинка
        Element imageElement = newElement.selectFirst("img");
        String image = "https://meteo7.ru" + imageElement.attr("src");
        // погода
        String weather = newElement.selectFirst("span[class=cl]").text() + ", "
                + newElement.selectFirst("span[class=pr]").text();
        // давление
        String pressure = newElement.selectFirst("td[class=pressure]").text();
        // влажность
        String humidity = newElement.selectFirst("td[class=humidity]").text();
        // ветер
        String wind;
        if (newElement.selectFirst("td[class=wind]") == null) {
            wind = newElement.selectFirst("td[class=wind caution]").text();
        } else {
            wind = newElement.selectFirst("td[class=wind]").text();
        }
        return new Weather(partText, image, temp, weather, pressure, humidity, wind);
    }

    public ArrayList<Weather> parsIt() throws IOException {
        ArrayList<Weather> weathers = new ArrayList<>();
        String url = "https://meteo7.ru/forecast/17074";
        Document page = Jsoup.parse(new URL(url), 3000);
        Elements table = page.select("table[class=m7-table]");

        for (Element currentElement : table) {
            Elements elements = currentElement.select("tr");
            for (Element newElement : elements) {
                if (newElement.selectFirst("td[class=partText]") == null) {
                    continue;
                }
                weathers.add(parseFromHTML(newElement, currentElement.selectFirst("caption").ownText()));
            }
        }

        return weathers;
    }
}
