package com.itcube.weather;

public class Weather {
    String title;
    String image;
    String temperature;
    String weather;
    String pressure;
    String humidity;
    String wind;

    public Weather(String title, String image, String temperature, String weather, String pressure, String humidity, String wind) {
        this.title = title;
        this.image = image;
        this.temperature = temperature;
        this.weather = weather;
        this.pressure = pressure;
        this.humidity = humidity;
        this.wind = wind;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "title='" + title + '\'' +
                ", image=" + image +
                ", temperature='" + temperature + '\'' +
                ", weather='" + weather + '\'' +
                ", pressure='" + pressure + '\'' +
                ", humidity='" + humidity + '\'' +
                ", wind='" + wind + '\'' +
                '}';
    }
}
