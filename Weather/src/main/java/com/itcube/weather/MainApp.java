package com.itcube.weather;

import java.io.IOException;
import java.util.ArrayList;

public class MainApp {
    public static void main(String[] args) throws IOException {
        ArrayList<Weather> weathers = new ArrayList<>();
        HTMLParser parser = new HTMLParser();
        WeatherFrame weatherFrame = new WeatherFrame();

        while (true) {
            weathers = parser.parsIt();
            weatherFrame.init(weathers);

            try {
                Thread.sleep(1300000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
