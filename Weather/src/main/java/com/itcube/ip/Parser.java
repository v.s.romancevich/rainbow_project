package com.itcube.ip;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.URL;

public class Parser {

    public void parsIt() throws IOException {
        String url = "https://whoer.net/ru";
        Document page = Jsoup.parse(new URL(url), 3000);
        Element elements = page.selectFirst("div[class=location-info container container_main-ip-info]");
        String ip = elements.selectFirst("div[class=button_icon your-ip]").text();
        System.out.println(ip);
    }
}
