package com.itcube.ip;

import java.io.IOException;

public class MainApp {
    public static void main(String[] args) throws IOException {
        Parser parser = new Parser();
        parser.parsIt();
    }
}
