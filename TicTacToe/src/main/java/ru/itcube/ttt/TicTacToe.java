package ru.itcube.ttt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicTacToe extends JFrame implements ActionListener {
    JButton newGameButton;
    JLabel score;
    JButton[] squares;
    int emptySquaresLeft = 9;

    public TicTacToe() {

        this.setTitle("�������� - ������");
        this.setSize(new Dimension(500, 500));
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setLayout(new BorderLayout());
        this.setBackground(Color.CYAN);

        Font comicSansMs = new Font("Comic Sans MS", Font.BOLD, 30);
        this.setFont(comicSansMs);

        newGameButton = new JButton("����� ����");
        newGameButton.addActionListener(this);

        JPanel topPanel = new JPanel();
        topPanel.add(newGameButton);
        this.add(topPanel, "North");

        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(3, 3));
        this.add(centerPanel, "Center");

        score = new JLabel("��� ���!");
        this.add(score, "South");

        squares = new JButton[9];

        for (int i = 0; i < 9; i++) {
            squares[i] = new JButton();
            squares[i].addActionListener(this);
            squares[i].setBackground(Color.ORANGE);
            squares[i].setFont(comicSansMs);
            centerPanel.add(squares[i]);
        }

        this.setVisible(true);
    }

    public static void main(String[] args) {
        TicTacToe newGame = new TicTacToe();
    }

    /**
     * ���� ����� ����� ������������ ��� �������
     *
     * @param e the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        JButton theButton = (JButton) e.getSource();
        // ��� ������ "����� ����"
        if (theButton == newGameButton) {
            for (int i = 0; i < 9; i++) {
                squares[i].setEnabled(true);
                squares[i].setText("");
                squares[i].setBackground(Color.ORANGE);
            }

            emptySquaresLeft = 9;
            score.setText("��� ���!");
            newGameButton.setEnabled(false);

            return; // ������� �� ������
        }

        String winner = "";
        // ��� ���� �� ������
        for (int i = 0; i < 9; i++) {
            if (theButton == squares[i]) {
                squares[i].setText("X");
                winner = lookForWinner();

                if (!"".equals(winner)) {
                    endTheGame();
                } else {
                    computerMove();
                    winner = lookForWinner();

                    if (!"".equals(winner)) {
                        endTheGame();
                    }
                }

                break;
            }
        }

        switch (winner) {
            case "X" -> score.setText("�� ��������!");
            case "O" -> score.setText("�� ���������!");
            case "T" -> score.setText("�����!");
        }
    }

    /**
     * ���� ����� ��������� ����� ������, ����� �����
     * ������ ������������ ���. ���� ������� ���
     * �� ������, ���������� ��������� ������.
     */
    private void computerMove() {
        int selectedSquare;
        // ������� ��������� �������� ����� ������ ������
        // ����� � ����� �������� � ��������, ����� ��������
        selectedSquare = findEmptySquare("O");

        // ���� �� �� ����� ����� ��� ������, �� ���� ��
        // ���������� �� ���� ��������� ������� ��� �� 3-�
        // ���������, �������� ����� ����� � ����� ����������
        if (selectedSquare == -1) {
            selectedSquare = findEmptySquare("X");
        }

        // ���� selectedSquare ��� ��� ����� -1, ��
        // ���������� ������ ����������� ������
        if (selectedSquare == -1 && squares[4].getText().equals("")) {
            selectedSquare = 4;
        }

        // �� ������� � ����������� �������...
        // ������ �������� ��������� ������
        if (selectedSquare == -1) {
            selectedSquare = getRandomSquare();
        }

        squares[selectedSquare].setText("O");
    }

    /**
     * ���� ����� �������� ����� ������ ������
     *
     * @return �������� ��������� ����� ������
     */
    private int getRandomSquare() {
        boolean gotEmptySquare = false;
        int selectedSquare;
        do {
            selectedSquare = (int) (Math.random() * 9);
            if (squares[selectedSquare].getText().equals("")) {
                gotEmptySquare = true;  // ����� ��������� ����
            }
        } while (!gotEmptySquare);

        return selectedSquare;
    }

    /**
     * ���� ����� ��������� ������ ���, ������� � ���������
     * ����� ������, ���� �� � ��� ��� ������
     * � ����������� ��������� � ������ �������
     *
     * @param player ���������� � - ��� ������������ � � - ��� ����������
     * @return ���������� ��������� ������,
     * ��� -1, ���� �� ������� ��� ������
     * � ����������� ���������
     */
    private int findEmptySquare(String player) {
        int[] weight = new int[9];
        for (int i = 0; i < 9; i++) {
            if (squares[i].getText().equals("O")) {
                weight[i] = -1;
            } else if (squares[i].getText().equals("X")) {
                weight[i] = 1;
            } else {
                weight[i] = 0;
            }
        }

        int twoWeights = player.equals("O") ? -2 : 2;

        // ��������, ���� �� � ���� 1 ��� ���������� ������ �
        // ���� ������.
        if (weight[0] + weight[1] + weight[2] == twoWeights) {
            if (weight[0] == 0) {
                return 0;
            } else if (weight[1] == 0) {
                return 1;
            } else {
                return 2;
            }
        }

        // ���������, ���� �� � ���� 2 ��� ���������� ������ �
        // ���� ������.
        if (weight[3] + weight[4] + weight[5] == twoWeights) {
            if (weight[3] == 0) {
                return 3;
            } else if (weight[4] == 0) {
                return 4;
            } else {
                return 5;
            }
        }

        // ���������, ���� �� � ���� 3 ��� ���������� ������ �
        // ���� ������.
        if (weight[6] + weight[7] + weight[8] == twoWeights) {
            if (weight[6] == 0) {
                return 6;
            } else if (weight[7] == 0) {
                return 7;
            } else {
                return 8;
            }
        }

        // ���������, ���� �� � ������� 1 ��� ���������� ������ �
        // ���� ������.
        if (weight[0] + weight[3] + weight[6] == twoWeights) {
            if (weight[0] == 0) {
                return 0;
            } else if (weight[3] == 0) {
                return 3;
            } else {
                return 6;
            }
        }

        // ���������, ���� �� � ������� 2 ��� ���������� ������ �
        // ���� ������.
        if (weight[1] + weight[4] + weight[7] == twoWeights) {
            if (weight[1] == 0) {
                return 1;
            } else if (weight[4] == 0) {
                return 4;
            } else {
                return 7;
            }
        }

        // ���������, ���� �� � ������� 3 ��� ���������� ������ �
        // ���� ������.
        if (weight[2] + weight[5] + weight[8] == twoWeights) {
            if (weight[2] == 0) {
                return 2;
            } else if (weight[5] == 0) {
                return 5;
            } else {
                return 8;
            }
        }

        // ���������, ���� �� � ��������� 1 ��� ���������� ������ �
        // ���� ������.
        if (weight[0] + weight[4] + weight[8] == twoWeights) {
            if (weight[0] == 0) {
                return 0;
            } else if (weight[4] == 0) {
                return 4;
            } else {
                return 8;
            }
        }

        // ���������, ���� �� � ��������� 2 ��� ���������� ������ �
        // ���� ������.
        if (weight[2] + weight[4] + weight[6] == twoWeights) {
            if (weight[2] == 0) {
                return 2;
            } else if (weight[4] == 0) {
                return 4;
            } else {
                return 6;
            }
        }
        // �� ������� ���� ���������� �������� ������
        return -1;
    }

    /**
     * ������ ������������ ������ � ��������� ������ "����� ����"
     */
    private void endTheGame() {
        newGameButton.setEnabled(true);

        for (int i = 0; i < 9; i++) {
            squares[i].setEnabled(false);
        }
    }

    /**
     * ���� ����� ���������� ����� ������� ����, ����� ������,
     * ���� �� ����������. �� ��������� ������ ���, �������
     * � ���������, ����� ����� ��� ������ � ����������� ���������
     * (�� �������)
     *
     * @return "�", "�", "�" - �����, "" - ��� ��� ����������
     */
    private String lookForWinner() {
        String theWinner = "";
        emptySquaresLeft--;

        if (emptySquaresLeft == 0) {
            return "T";     // ��� �����. "�" - �� ����������� ����� tie
        }
        // ��������� ��� 1 - �������� ������� 0, 1, 2
        if (!squares[0].getText().equals("") &&
                squares[0].getText().equals(squares[1].getText()) &&
                squares[0].getText().equals(squares[2].getText())) {
            theWinner = squares[0].getText();
            highlightWinner(0, 1, 2);

            // ��������� ��� 2 - �������� ������� 3, 4, 5
        } else if (!squares[3].getText().equals("") &&
                squares[3].getText().equals(squares[4].getText()) &&
                squares[3].getText().equals(squares[5].getText())) {
            theWinner = squares[3].getText();
            highlightWinner(3, 4, 5);

            // ��������� ��� 3 - �������� ������� 6, 7, 8
        } else if (!squares[6].getText().equals("") &&
                squares[6].getText().equals(squares[7].getText()) &&
                squares[6].getText().equals(squares[8].getText())) {
            theWinner = squares[6].getText();
            highlightWinner(6, 7, 8);

            // ��������� ������� 1 - �������� ������� 0, 3, 6
        } else if (!squares[0].getText().equals("") &&
                squares[0].getText().equals(squares[3].getText()) &&
                squares[0].getText().equals(squares[6].getText())) {
            theWinner = squares[0].getText();
            highlightWinner(0, 3, 6);

            // ��������� ������� 2 - �������� ������� 1, 4, 7
        } else if (!squares[1].getText().equals("") &&
                squares[1].getText().equals(squares[4].getText()) &&
                squares[1].getText().equals(squares[7].getText())) {
            theWinner = squares[1].getText();
            highlightWinner(1, 4, 7);

            // ��������� ������� 3 - �������� ������� 2, 5, 8
        } else if (!squares[2].getText().equals("") &&
                squares[2].getText().equals(squares[5].getText()) &&
                squares[2].getText().equals(squares[8].getText())) {
            theWinner = squares[2].getText();
            highlightWinner(2, 5, 8);

            // ��������� ������ ��������� - �������� ������� 0, 4, 8
        } else if (!squares[0].getText().equals("") &&
                squares[0].getText().equals(squares[4].getText()) &&
                squares[0].getText().equals(squares[8].getText())) {
            theWinner = squares[0].getText();
            highlightWinner(0, 4, 8);

            // ��������� ������ ��������� - �������� ������� 2, 4, 6
        } else if (!squares[2].getText().equals("") &&
                squares[2].getText().equals(squares[4].getText()) &&
                squares[2].getText().equals(squares[6].getText())) {
            theWinner = squares[2].getText();
            highlightWinner(2, 4, 6);
        }

        return theWinner;
    }

    /**
     * ���� ����� �������� ���������� �����.
     *
     * @param win1 ������ ������ ��� ���������.
     * @param win2 ������ ������ ��� ���������.
     * @param win3 ������ ������ ��� ���������.
     */
    private void highlightWinner(int win1, int win2, int win3) {
        squares[win1].setBackground(Color.CYAN);
        squares[win2].setBackground(Color.CYAN);
        squares[win3].setBackground(Color.CYAN);
    }
}
